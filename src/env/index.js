const envValues = {
  BASE_URL_API: "https://jsonplaceholder.typicode.com",
  BASE_URL_DEV: "http://localhost:8080",

  role: {
    ouvrier: ["ouvrier", "commercial", "admin"],
    commercial: ["commercial", "admin"],
    admin: ["admin"],
  },
};

export default envValues;
