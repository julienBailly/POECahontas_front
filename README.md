# POECAHONTAS FRONT

En apesanteur le Musk du raton n'a pas d'odeur

![Logo](src/assets/img/logo.png)

## Project setup

```
pnpm install
```

### Compiles and hot-reloads for development

```
pnpm run serve
```

### Compiles and minifies for production

```
pnpm run build
```

### Lints and fixes files

```
pnpm run lint
```

### Docs/Manuels

[Librairie d'icones animées](https://epic-spinners.epicmax.co/).

[Librairie d'icones fixes](https://www.npmjs.com/package/vue-material-design-icons)

[CLI Vue](https://cli.vuejs.org/config/).
